import random
import arcade
import os

# --- Constants ---
SPRITE_SCALING_PLAYER = 0.8
SPRITE_SCALING_COIN = 0.2
COIN_COUNT = 10
MOVEMENT_SPEED = 8

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Usbzbierator"


class SuperZoombie(arcade.Sprite):
    def __init__(self,
                 filename: str = None,
                 scale: float = 1,
                 image_x: float = 0, image_y: float = 0,
                 image_width: float = 0, image_height: float = 0,
                 center_x: float = 0, center_y: float = 0,
                 repeat_count_x: int = 1, repeat_count_y: int = 1):
        super().__init__(filename,
                         scale,
                         image_x, image_y,
                         image_width, image_height,
                         center_x, center_y,
                         repeat_count_x, repeat_count_y)
        self.change_x = 0

    def update(self):
        self.center_x += self.change_x

        if self.center_x < self.width/2:
            self.center_x = self.width/2

        if self.center_x > SCREEN_WIDTH - self.width/2:
            self.center_x = SCREEN_WIDTH - self.width/2


class Coin(arcade.Sprite):
    def reset_pos(self):
        self.center_y = random.randrange(SCREEN_HEIGHT + 20,
                                         SCREEN_HEIGHT + 100)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        self.center_y -= 1
        if self.top < 0:
            self.reset_pos()


class MyGame(arcade.Window):

    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        file_path = os.path.dirname(os.path.abspath(__file__))
        os.chdir(file_path)

        self.player_sprite_list = None
        self.coin_sprite_list = None

        self.player_sprite = None
        self.score = 0

        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        self.player_sprite_list = arcade.SpriteList()
        self.coin_sprite_list = arcade.SpriteList()
        self.score = 0
        self.player_sprite = SuperZoombie(":resources:images/animated_characters/zombie/zombie_idle.png",
                                   SPRITE_SCALING_PLAYER)

        self.player_sprite.center_x = 50
        self.player_sprite.center_y = 50
        self.player_sprite_list.append(self.player_sprite)

        self.create_coins()

    def create_coins(self):
        for i in range(COIN_COUNT):
            self.create_single_coin()

    def create_single_coin(self):
        coin = Coin("usb.png", SPRITE_SCALING_COIN)
        coin.center_y = random.randrange(SCREEN_HEIGHT + 20,
                                         SCREEN_HEIGHT + 100)
        coin.center_x = random.randrange(SCREEN_WIDTH)
        self.coin_sprite_list.append(coin)

    def on_draw(self):
        arcade.start_render()
        self.coin_sprite_list.draw()
        self.player_sprite_list.draw()

        output = f"Wynik: {self.score}"
        arcade.draw_text(output, 10, 20, arcade.color.WHITE, 14)

    def on_key_press(self, key, modifiers):
        if key == arcade.key.LEFT or key == arcade.key.A:
            self.player_sprite.change_x = -MOVEMENT_SPEED

        if key == arcade.key.RIGHT or key == arcade.key.D:
            self.player_sprite.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        if key == arcade.key.LEFT or key == arcade.key.RIGHT\
                or key == arcade.key.A or key == arcade.key.D:
            self.player_sprite.change_x = 0

    def on_update(self, delta_time):
        self.coin_sprite_list.update()
        self.player_sprite_list.update()
        hit_list = arcade.check_for_collision_with_list(self.player_sprite,
                                                        self.coin_sprite_list)
        for coin in hit_list:
            coin.remove_from_sprite_lists()
            self.score += 1
            self.create_single_coin()


def main():
    window = MyGame()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
